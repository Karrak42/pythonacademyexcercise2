class Snake:
    def __init__(self):
        self.snake_size: int = 20
        self.snake_name: str = "snake"


class Cobra(Snake):
    def __init__(self):
        super().__init__()
        print("Cobra {} is {} cm long".format(self.snake_name, self.snake_size))


class Animal:
    def __init__(self):
        print("I'm an Animal")

    def eat_food(self) -> str:
        print("Consume food")


class Human(Animal):
    def __init__(self):
        super().__init__()
        print("I'm a Human")


class Computer:
    def __init__(self):
        pass

    def turn_on(self) -> None:
        print("The computer is turned on")


class ApplePC(Computer):
    def __init__(self):
        super().__init__()

    def turn_on(self) -> None:
        print("The Apple pc is turned on")

    def normal_turn_on(self) -> None:
        super().turn_on()


