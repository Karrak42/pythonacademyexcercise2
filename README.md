# Excercise_2 README.md

## Excercise1.py

### Contains three classes witch do the following:

##### Class *Cobra* inherits varibles from class Snake

##### Class *Human* inherits methods from class Animal

##### Class *ApplePC* inherits varibles and methods from class Computer and Overides the method *turn_on*

## Excersise2.py 

### Contains two funtion and one class witch do the following:

##### Funtion *user_information* takes three inputs:
##### - user_name
##### - user_surname
##### - user_age

##### And returns them as string to the user

##### Funtion *to_int_converter* converts a float number to int and prints the string with the answer

##### Class *EveryPlant* takes three values in the construtor:

##### - plant_name
##### - plant_color
##### - number_code

##### And returns them as string to user

## Excersise3.py

### Contains one function and 8 classes that do the following things:

##### function *name_checker* takes input name and when the name is the same as mine("Maciej") returns a string
##### otherwise returs a normal string with the users name

##### Class *Rectangle* create an Rectangle object and overrides comaprison operators to compare objcets by the area they take

##### Class *Cube* creates a Cube object and ovverides comaprison operator to compare objcets by the Volume they take

##### Class rest of the classes are a class tree where they inherit in this order

##### Vehicle -> Car
#####	    -> MotorBike

##### Car -> Volkswagen
#####	-> BMW

##### MotorBike -> Harley
#####	      -> Yamaha 

##### All the rest of the classes have overriden comaprison operator so you can compare the objects by:

##### Vehicle: wheel count
##### Car and MotorBike: horse power
##### Volkswagen,BMW,Harley,Yamaha: speed

