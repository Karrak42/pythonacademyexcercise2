def name_checker() -> str:
    name = input("What's your name: ")

    if name == "Maciej":
        return "Hello Maciej we have the same name"
    else:
        return "Hello {} have a nice day".format(name)


class Rectangle:
    def __init__(self, rec_width: int, rec_height: int):
        self.rec_width = rec_width
        self.rec_height = rec_height

    def rec_area(self) -> int:
        return self.rec_width * self.rec_height

    def __eq__(self, other):
        return self.rec_area() == other.rec_area()


class Cube:
    def __init__(self, edge: int):
        self.edge = edge

    def cube_volume(self):
        return pow(self.edge, 3)

    def __eq__(self, other):
        return self.cube_volume() == other.cube_volume()

    def __lt__(self, other):
        return self.cube_volume() < other.cube_volume()

    def __gt__(self, other):
        return self.cube_volume() > other.cube_volume()


class Vehicle:
    def __init__(self, horse_power: int, wheels_count: int, speed: int):
        self.horse_power = horse_power
        self.wheels_count = wheels_count
        self.speed = speed
        print("I'm an Vehicle ")

    def __eq__(self, other):
        return self.wheels_count == other.wheels_count and self.horse_power == other.horse_power and self.speed == other.speed


class MotorBike(Vehicle):
    def __init__(self, horse_power: int, wheels_count: int, speed: int, engine_type: str):
        self.engine_type = engine_type
        super().__init__(horse_power, wheels_count, speed)
        print("I'm a motorbike")

    def __eq__(self, other):
        return super().__eq__(other) and self.engine_type == other.engine_type


class Car(Vehicle):
    def __init__(self, horse_power: int, wheels_count: int, speed: int, color: str):
        super().__init__(horse_power, wheels_count, speed)
        self.color = color
        print("I'm a car")

    def __eq__(self, other):
        return super().__eq__(other) and self.color == other.color


class Volkswagen(Car):
    def __init__(self, horse_power: int, wheels_count: int, speed: int, color: str):
        super().__init__(horse_power, wheels_count, speed, color)
        print("I'm a Volkswagen car")


class BMW(Car):
    def __init__(self, horse_power: int, wheels_count: int, speed: int, color: str):
        super().__init__(horse_power, wheels_count, speed, color)
        print("I'm a BMW car")


class Harley(MotorBike):
    def __init__(self, horse_power: int, wheels_count: int, speed: int, engine_type: str):
        super().__init__(horse_power, wheels_count, speed, engine_type)
        print("I'm a Harley motorbike")


class Yamaha(MotorBike):
    def __init__(self, horse_power: int, wheels_count: int, speed: int, engine_type: str):
        super().__init__(horse_power, wheels_count, speed, engine_type)
        print("I'm a Yamaha motorbike")



