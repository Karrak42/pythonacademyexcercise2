def user_information() -> str:
    user_name = input("What's your name? ")
    user_surname = input("What's your surname? ")
    user_age = input("How old are you? ")

    return "You are {} {} and you are {} years old".format(user_name, user_surname, user_age)


def to_int_converter() -> str:
    user_float = input("Give a float number: ")

    return "Your converted number equals: {}".format(int(float(user_float)))


class EveryPlant:
    def __init__(self, plant_name: str, plant_color: str, number_code: int):
        self.plant_name = plant_name
        self.plant_color = plant_color
        self.number_code = number_code

        print("Your plant is named {} is {} and has a ID number equal {}".format(self.plant_name, self.plant_color, self.number_code))
